<?php

declare(strict_types=1);

namespace App\Tests\Category;

use App\Category\Category;
use App\Category\CategoryIterator;
use App\Category\CategoryTree;
use PHPUnit\Framework\TestCase;

class CategoryTreeTest extends TestCase
{
    public function testGetTree()
    {
        $categories = [
            new Category(1, 'Zdrowie'),
            new Category(2, 'Sport'),
            new Category(3, 'Uroda'),
        ];

        $categoryIterator = new CategoryIterator();
        foreach ($categories as $category) {
            $categoryIterator->addCategory($category);
        }

        $tree = [
                [
                    'id' => 1,
                    'children' => [
                        [
                             'id' => 2,
                            'children' => [],
                         ],
                    ],
                ],
                [
                    'id' => 3,
                    'children' => [
                        [
                            'id' => 1,
                            'children' => [],
                        ],
                    ],
                ],
        ];

        $expectedTree = [
            [
                'id' => 1,
                'name' => 'Zdrowie',
                'children' => [
                    [
                        'id' => 2,
                        'name' => 'Sport',
                        'children' => [],
                    ],
                ],
            ],
            [
                'id' => 3,
                'name' => 'Uroda',
                'children' => [
                    [
                        'id' => 1,
                        'name' => 'Zdrowie',
                        'children' => [],
                    ],
                ],
            ],
        ];

        $categoryTree = new CategoryTree($categoryIterator, $tree);
        $tree = $categoryTree->getTree();

        $this->assertEquals($expectedTree, $tree);
    }
}
