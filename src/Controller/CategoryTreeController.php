<?php

declare(strict_types=1);

namespace App\Controller;

use App\Category\CategoryTreeService;
use App\Form\CategoryTreeFileModel;
use App\Form\CategoryTreeFileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryTreeController extends AbstractController
{
    /** @var CategoryTreeService */
    private $categoryTreeService;

    public function __construct(CategoryTreeService $categoryTreeService)
    {
        $this->categoryTreeService = $categoryTreeService;
    }

    /**
     * @Route("/", name="category_tree_load_files")
     */
    public function loadFiles(Request $request)
    {
        $form = $this->createForm(CategoryTreeFileType::class, $categoryTreeFile = new CategoryTreeFileModel());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listFile = $categoryTreeFile->getListFile()->getRealPath();
            $treeFile = $categoryTreeFile->getTreeFile()->getRealPath();

            try {
                return $this->json($this->categoryTreeService->getTreeFromFiles($treeFile, $listFile));
            } catch (\Exception $e) {
                return $this->json(['error' => 'Passed invalid files: ' . $e->getMessage()], 400);
            }
        }

        return $this->render('category_tree_files.html.twig', ['form' => $form->createView()]);
    }
}
