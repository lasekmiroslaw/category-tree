<?php

declare(strict_types=1);

namespace App\Category;

class CategoryTree
{
    /** @var array */
    private $tree;

    /** @var CategoryIterator */
    private $categories;

    public function __construct(CategoryIterator $categories, array $tree)
    {
        $this->tree = $tree;
        $this->categories = $categories;
    }

    public function getTree(): array
    {
        $this->addCategory($this->tree, [$this, 'correlateIdWithCategory']);

        return $this->tree;
    }

    private function correlateIdWithCategory(array &$treeItem): void
    {
        $categoryKey = $treeItem['id'];
        $category = $this->categories->getCategoryById($categoryKey);
        if ($category !== null) {
            $treeItem['name'] = $category->getName();
        }

        if (isset($treeItem['children']) && !empty($treeItem['children'])) {
            $this->addCategory($treeItem['children'], [$this, 'correlateIdWithCategory']);
        }
    }

    private function addCategory(array &$tree, callable $correlateFunction): void
    {
        array_walk($tree, $correlateFunction);
    }
}
