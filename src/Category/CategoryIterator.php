<?php

declare(strict_types=1);

namespace App\Category;

final class CategoryIterator implements \IteratorAggregate
{
    /** @var Category[] */
    private $categories;

    public function addCategory(Category $category): void
    {
        $this->categories[$category->getId()] = $category;
    }

    public function getCategoryById(int $id): ?Category
    {
        if (isset($this->categories[$id])) {
            return $this->categories[$id];
        }

        return null;
    }

    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->categories);
    }
}
