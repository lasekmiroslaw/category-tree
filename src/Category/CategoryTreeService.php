<?php

declare(strict_types=1);

namespace App\Category;

use App\Util\JsonFileReader;

class CategoryTreeService
{
    /** @var JsonFileReader */
    private $jsonFileReader;

    /** @var CategoryService */
    private $categoryService;

    public function __construct(JsonFileReader $fileReader, CategoryService $categoryService)
    {
        $this->jsonFileReader = $fileReader;
        $this->categoryService = $categoryService;
    }

    public function getTreeFromFiles(string $treeFile, string $categoryFile): array
    {
        $decodedTree = $this->jsonFileReader->decodeToArray($treeFile);
        $decodedList = $this->jsonFileReader->decodeToArray($categoryFile);

        $categories = $this->categoryService->getCategories($decodedList);
        $categoryTree = new CategoryTree($categories, $decodedTree);

        return $categoryTree->getTree();
    }
}
