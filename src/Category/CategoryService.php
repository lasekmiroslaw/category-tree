<?php

declare(strict_types=1);

namespace App\Category;

class CategoryService
{
    /** @var CategoryIterator */
    private $categories;

    public function __construct()
    {
        $this->categories = new CategoryIterator();
    }

    /**
     * @throws \UnexpectedValueException
     */
    public function getCategories(array $categoryArr): CategoryIterator
    {
        return $this->getCategoriesWithName($categoryArr);
    }

    /**
     * @throws \UnexpectedValueException
     */
    private function getCategoriesWithName(array $categoryArr): CategoryIterator
    {
        foreach ($categoryArr as $category) {
            $category = $this->validateCategory($category);
            $this->categories->addCategory(new Category((int) $category['category_id'], $category['translations']['pl_PL']['name']));
        }

        return $this->categories;
    }

    /**
     * @throws \UnexpectedValueException
     */
    private function validateCategory(array $category): array
    {
        if (!isset($category['category_id'])) {
            throw new \UnexpectedValueException('Passed invalid category, category_id is not defined');
        }

        if (!isset($category['translations']['pl_PL']['name'])) {
            throw new \UnexpectedValueException('Passed invalid category, translations name is not defined\'');
        }

        return $category;
    }
}
