<?php

declare(strict_types=1);

namespace App\Util;

class JsonFileReader
{
    private const MAX_TREE_DEPTH = 1000;

    /**
     * @throws \JsonException
     * @throws \InvalidArgumentException
     */
    public function decodeToArray(string $file, $maxDepth = self::MAX_TREE_DEPTH): array
    {
        if (!is_file($file)) {
            throw new \InvalidArgumentException('Passed string is not a valid file');
        }
        $json = file_get_contents($file);

        return json_decode($json, true, $maxDepth, \JSON_THROW_ON_ERROR);
    }
}
