<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\HttpFoundation\File\File;

class CategoryTreeFileModel
{
    /** @var ?File */
    private $listFile;

    /** @var ?File */
    private $treeFile;

    public function getListFile(): ?File
    {
        return $this->listFile;
    }

    public function setListFile(?File $listFile): void
    {
        $this->listFile = $listFile;
    }

    public function getTreeFile(): ?File
    {
        return $this->treeFile;
    }

    public function setTreeFile(?File $treeFile): void
    {
        $this->treeFile = $treeFile;
    }
}
