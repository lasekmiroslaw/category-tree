# category-tree

## Installation
Run scripts:
```
docker-compose up --b --d
docker-compose exec php-fpm bash
composer install
```

## Tests
./bin/phpunit


## Usage

go to http://localhost:8000 and
load files:
* /example/list.json
* /example/tree.json
